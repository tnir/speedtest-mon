FROM alpine:3.12
MAINTAINER takninnovationresearch@gmail.com

RUN apk --update add python3 py3-pip \
  && pip3 install speedtest-cli==1.0.7 \
  && rm -rf /var/cache/apk/*

CMD ["/usr/bin/speedtest-cli", "--list"]
